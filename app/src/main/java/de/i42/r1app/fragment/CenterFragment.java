package de.i42.r1app.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import de.i42.r1app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CenterFragment extends Fragment implements View.OnClickListener {

    ImageButton button1;
    ImageButton button2;

    public CenterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_center, container, false);

        button1 = rootView.findViewById(R.id.button1);
        button1.setOnClickListener(this);

        button2 = rootView.findViewById(R.id.button2);
        button2.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        String url = null;

        if (view == button1) {
            url = getResources().getString(R.string.url_button1);
        }
        else if (view == button2) {
            url = getResources().getString(R.string.url_button2);
        }

        if (url != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        // React when the mapFragment is going on top of the show stack
//        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//                Fragment topMostFragment = Navigation.getTopMostFragment(getActivity());
//                if (topMostFragment instanceof DummyFragment) {
//                    //updateConnectionBoxes();
//                }
//            }
//        });
//    }
}