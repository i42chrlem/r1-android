package de.i42.r1app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import de.i42.r1app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RightFragment extends Fragment {

    public RightFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_left, container, false);

        return rootView;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        // React when the mapFragment is going on top of the show stack
//        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//                Fragment topMostFragment = Navigation.getTopMostFragment(getActivity());
//                if (topMostFragment instanceof DummyFragment) {
//                    //updateConnectionBoxes();
//                }
//            }
//        });
//    }
}