package de.i42.r1app.menu;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import de.i42.mdm.notfallapp.core.activity.MenuWebTextActivity;
import de.i42.mdm.notfallapp.core.activity.PersonalDataActivity;
import de.i42.mdm.notfallapp.core.menu.DrawerNavigationItem;
import de.i42.mdm.notfallapp.core.menu.DrawerNavigationItemProvider;
import de.i42.r1app.R;
import de.i42.r1app.util.MDMConfig;


public class DrawerNavigationItemProviderImpl implements DrawerNavigationItemProvider {

    public List<DrawerNavigationItem> getMenuList(Context context) {
        List<DrawerNavigationItem> drawerNavigationItems = new ArrayList<>();
        MDMConfig config = MDMConfig.getInstance(context);

        // Settings
        Intent personalDataIntent = new Intent(context, PersonalDataActivity.class);
        //personalDataIntent.putExtra(config.getExtraMenuHeader(), "Persönliche Daten");
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getString(R.string.nav_settings),
                null, //personalDataIntent,
                0,
                0,
                1,
                ""
        ));

        // Bluetooth list
        /*Intent bluetoothIntent = new Intent(context, BluetoothActivity.class);
        drawerNavigationItems.add(new DrawerNavigationItem(
                "Bluetooth",
                bluetoothIntent,
                1,
                1,
                1,
                ""
        )); */

        //TODO: Refactor this
        Class activityClass = MenuWebTextActivity.class;

        // Hint and tips
        Intent tipsAndTricksIntent = null; //new Intent(context, activityClass);
        //tipsAndTricksIntent.putExtra(config.getExtraMenuHeader(), context.getString(R.string.menu_hints_and_tips));
        //tipsAndTricksIntent.putExtra(config.getExtraMenuTextResource(), R.raw.tips_and_tricks);
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getResources().getString(R.string.nav_hints_and_tips),
                tipsAndTricksIntent,
                1,
                1,
                2,
                ""
        ));

        // Terms of use
        Intent termsOfUseIntent = null; //new Intent(context, activityClass);
        //termsOfUseIntent.putExtra(config.getExtraMenuHeader(), context.getString(R.string.menu_terms_of_use));
        //termsOfUseIntent.putExtra(config.getExtraMenuTextResource(), R.raw.terms_of_use);
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getResources().getString(R.string.nav_terms_of_use),
                termsOfUseIntent,
                2,
                2,
                2,
                ""
        ));

        // Legal
        Intent legalIntent = null; //new Intent(context, activityClass);
        //legalIntent.putExtra(config.getExtraMenuHeader(), context.getString(R.string.menu_legal));
        //legalIntent.putExtra(config.getExtraMenuTextResource(), R.raw.legal);
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getResources().getString(R.string.nav_legal),
                legalIntent,
                3,
                3,
                2,
                ""
        ));


        // About us
        Intent aboutIntent = null; //new Intent(context, activityClass);
        //aboutIntent.putExtra(config.getExtraMenuHeader(), context.getString(R.string.menu_about_us_title));
        //aboutIntent.putExtra(config.getExtraMenuTextResource(), R.raw.about_us);
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getResources().getString(R.string.nav_about),
                aboutIntent,
                4,
                4,
                2,
                ""
        ));

        // Imprint

        String imprintContent = "";
        String aboutUs = "";
        Intent imprintIntent = null; //new Intent(context, activityClass);
        //imprintIntent.putExtra(config.getExtraMenuHeader(), context.getString(R.string.menu_imprint_title));
        //imprintIntent.putExtra(config.getExtraMenuTextResource(), R.raw.imprint);
        drawerNavigationItems.add(new DrawerNavigationItem(
                context.getResources().getString(R.string.nav_imprint),
                imprintIntent,
                5,
                5,
                2,
                ""
        ));


        // Logout
        /*if(LoginManager.getInstance().isLoggedIn(context)) {
            drawerNavigationItems.add(new DrawerNavigationItem(
                    context.getResources().getString(R.string.nav_logout),
                    new Intent(context, LogoutActivity.class),
                    3,
                    3
            ));
        }

        // Login
        else {
            drawerNavigationItems.add(new DrawerNavigationItem(
                    context.getResources().getString(R.string.nav_login),
                    new Intent(context, LoginActivity.class),
                    4,
                    4
            ));
        } */


        return drawerNavigationItems;
    }
}
