package de.i42.r1app.util;

import android.content.Context;

import de.i42.mdm.notfallapp.core.util.GlobalConfig;

public class MDMConfig extends GlobalConfig {
    private static MDMConfig ourInstance;

    public static MDMConfig getInstance(Context context) {
        if(ourInstance == null) {
            ourInstance = new MDMConfig(context);
        }
        return ourInstance;
    }

    private MDMConfig(Context context) {
        super(context);
    }

    public static final String TAG = "R1";
}
