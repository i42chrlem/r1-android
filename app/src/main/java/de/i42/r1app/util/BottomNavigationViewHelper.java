package de.i42.r1app.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

public class BottomNavigationViewHelper {

    @SuppressLint("RestrictedApi")
    public static void disableShiftModeAndFixPadding(BottomNavigationMenuView menuView) {
        int childsCount = menuView.getChildCount();
        float scale = menuView.getContext().getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (16*scale + 0.5f);

        for(int i = 0; i < childsCount; ++i) {
            BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
            item.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED);
            item.setPadding(0, dpAsPixels, 0, 0);
        }
    }

    public static void resizeMenuItem(View iconView, int width, int height, Context context) {
        final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
        layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, displayMetrics);
        iconView.setLayoutParams(layoutParams);
    }
}
