package de.i42.r1app.tabbar;

import androidx.fragment.app.Fragment;

import java.util.List;

public interface TabbarItemProvider {

    List<TabbarButton> getTabbarItems();
    TabbarButtonBig getBigButton();
    Fragment getInitialFragment();
}
