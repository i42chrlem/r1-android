package de.i42.r1app.tabbar;

import android.content.Context;

public interface TabbarNavigationListener {
    void onTabbarButtonClick(TabbarButton tabbarButton, Context context);
    void onBigTabbarButtonClick(TabbarButton bigTabbarButton);
}
