package de.i42.r1app.tabbar;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import de.i42.r1app.R;
import de.i42.r1app.fragment.LeftFragment;

public class TabbarItemProviderImpl implements TabbarItemProvider {

    TabbarButtonBig bigButton;
    List<TabbarButton> tabbarButtons;
    Fragment initalFragment;

    public TabbarItemProviderImpl() {
        //setup(null);
        setup();
    }

//    public TabbarItemProviderImpl(OnMapPositionChangeListener mapPositionChangeListener) {
//        setup(mapPositionChangeListener);
//    }


    //private void setup(OnMapPositionChangeListener mapPositionChangeListener) {
    private void setup() {
        bigButton = new TabbarButtonBig(
                //R.drawable.selector_tabbbar_center, 20, R.drawable.tabbar_call_selected, R.drawable.tabbar_call, 80, 80);
                R.drawable.selector_tabbbar_item_center, 20, R.drawable.tabbar_item_center, R.drawable.tabbar_item_center, 80, 80);

        tabbarButtons = new ArrayList<TabbarButton>() {{
            //add(new TabbarButton(R.drawable.item_tabbar_archive, 0, R.drawable.item_tabbar_archive, 32, 30));
            add(new TabbarButton(R.drawable.selector_tabbar_item_left, 0, R.drawable.selector_tabbar_item_left, 53, 46));
            //////add(new TabbarButton(R.drawable.item_tabbar_checkin, 10, R.drawable.item_tabbar_checkin, 30, 30));
            add(bigButton);
            //////add(new TabbarButton(R.drawable.item_tabbar_tracking, 30, R.drawable.item_tabbar_tracking, 30, 30));
            //add(new TabbarButton(R.drawable.selector_tabbar_item_right, 40, R.drawable.selector_tabbar_item_right, 35, 30));
            add(new TabbarButton(R.drawable.selector_tabbar_item_right, 40, R.drawable.selector_tabbar_item_right, 58, 42));
        }};

        initalFragment = new LeftFragment();
    }

    @Override
    public List<TabbarButton> getTabbarItems() {

        return tabbarButtons;
    }

    @Override
    public TabbarButtonBig getBigButton() {
        return bigButton;
    }

    @Override
    public Fragment getInitialFragment() {
        return initalFragment;
    }
}
