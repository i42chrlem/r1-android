package de.i42.r1app.tabbar;

public class TabbarButtonBig extends TabbarButton implements TabbarNavigationItemBig {

    int activeResourceId;
    int inactiveResrouceId;
    boolean active;

    public TabbarButtonBig(int id, int order, int activeResourceId, int inactiveResourceId, int iconWidth, int iconHeight) {
        super(id, order, inactiveResourceId, iconWidth, iconHeight);

        this.activeResourceId = activeResourceId;
        this.inactiveResrouceId = inactiveResourceId;
    }

    @Override
    public void setActive() {
        active = true;
    }

    @Override
    public void setInactive() {
        active = false;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public int getActiveResourceId() {
        return activeResourceId;
    }

    @Override
    public int getInactiveResourceId() {
        return inactiveResrouceId;
    }
}
