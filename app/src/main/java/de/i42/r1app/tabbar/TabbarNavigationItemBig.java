package de.i42.r1app.tabbar;

public interface TabbarNavigationItemBig extends TabbarNavigationItem {

    void setActive();
    void setInactive();
    boolean isActive();

    int getActiveResourceId();
    int getInactiveResourceId();
}
