package de.i42.r1app.tabbar;

public interface TabbarNavigationItem {

    int getId();
    String getTitle();
    int getOrder();
    int getDrawableRecourceId();

    int getIconWidth();
    int getIconHeight();
}
