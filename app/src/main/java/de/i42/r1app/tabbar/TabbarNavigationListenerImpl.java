package de.i42.r1app.tabbar;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;

import org.greenrobot.eventbus.EventBus;

import de.i42.mdm.notfallapp.core.event.ShowFragmentNavigationEvent;
import de.i42.r1app.R;
import de.i42.r1app.fragment.CenterFragment;
import de.i42.r1app.fragment.LeftFragment;
import de.i42.r1app.fragment.RightFragment;

public class TabbarNavigationListenerImpl implements TabbarNavigationListener {
    @Override
    public void onTabbarButtonClick(TabbarButton tabbarButton, Context context) {
        Fragment fragmentToPush = null;
        switch(tabbarButton.getId()) {
            case R.drawable.selector_tabbar_item_left:
                fragmentToPush = new LeftFragment(); //ArchiveFragment();
                break;
//            case R.drawable.item_tabbar_checkin:
//                fragmentToPush = new DummyFragment(); //CheckInFragment();
//                break;
            case R.drawable.selector_tabbbar_item_center:
                fragmentToPush = new CenterFragment(); //EmergencyFragment.newInstance(new Pinger(context), "004962154901878");
                break;
//            case R.drawable.item_tabbar_tracking:
//                fragmentToPush = new DummyFragment(); //TrackingFragment();
//                break;
            case R.drawable.selector_tabbar_item_right:
                fragmentToPush = null; //new RightFragment(); //NewsFragment();
                break;
        }

        if(fragmentToPush != null) {
            EventBus.getDefault().post(new ShowFragmentNavigationEvent(fragmentToPush));
        }
    }

    @Override
    public void onBigTabbarButtonClick(TabbarButton bigTabbarButton) {
        Log.d("MainActivty", "BIG BUTTON PRESSED");
    }
}
