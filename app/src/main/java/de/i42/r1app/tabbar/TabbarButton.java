package de.i42.r1app.tabbar;

public class TabbarButton implements TabbarNavigationItem {

    int id;
    String title;
    int order;
    int drawableResourceId;
    int iconWidth;
    int iconHeight;

    public TabbarButton(int id, int order, int drawableResourceId, int iconWidth, int iconHeight) {
        this.id = id;
        this.title = "";
        this.order = order;
        this.drawableResourceId = drawableResourceId;
        this.iconWidth = iconWidth;
        this.iconHeight = iconHeight;
    }

    public TabbarButton(int id, String title, int order, int drawableResourceId, int iconWidth, int iconHeight) {
        this.id = id;
        this.title = title;
        this.order = order;
        this.drawableResourceId = drawableResourceId;
        this.iconWidth = iconWidth;
        this.iconHeight = iconHeight;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public int getDrawableRecourceId() {
        return drawableResourceId;
    }

    @Override
    public int getIconWidth() {
        return iconWidth;
    }

    @Override
    public int getIconHeight() {
        return iconHeight;
    }
}
