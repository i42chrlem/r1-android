package de.i42.r1app.activity.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import de.i42.r1app.R;

public class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;
    RelativeLayout contentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);

        contentContainer = findViewById(R.id.base_content_container);

        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            //getSupportActionBar().setHomeButtonEnabled(true);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    // --------------------------------------------------------------------------------------------
    //  Set/Add Content View
    // --------------------------------------------------------------------------------------------

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(layoutResID, null);
        _setContentView(view, getDefaultLayoutParams());
    }

    @Override
    public void setContentView(View view) {
        _setContentView(view, getDefaultLayoutParams());
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        _setContentView(view, params);
    }

    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params) {
        _setContentView(view, params);
    }

    private ViewGroup.LayoutParams getDefaultLayoutParams() {
        return new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void _setContentView(View view, ViewGroup.LayoutParams params) {
        if(contentContainer == null) {
            throw new NullPointerException("BaseDrawerActivity: 'contentContainer' is NULL. You need to call BaseDrawerActivity.onCreate() first, before you can do any layout changes.");
        }
        contentContainer.addView(view, params);
    }
}
