package de.i42.r1app.activity;

import android.os.Bundle;
import android.view.KeyEvent;

import androidx.fragment.app.Fragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import de.i42.mdm.notfallapp.core.event.ShowFragmentNavigationEvent;
import de.i42.mdm.notfallapp.core.util.Config;
import de.i42.mdm.notfallapp.core.util.GlobalConfig;
import de.i42.mdm.notfallapp.core.util.Navigation;
import de.i42.mdm.notfallapp.core.util.Preferences;
import de.i42.r1app.R;
import de.i42.r1app.activity.base.BaseDrawerActivity;
import de.i42.r1app.tabbar.TabbarButton;
import de.i42.r1app.tabbar.TabbarItemProvider;
import de.i42.r1app.tabbar.TabbarItemProviderImpl;
import de.i42.r1app.tabbar.TabbarNavigationListener;
import de.i42.r1app.tabbar.TabbarNavigationListenerImpl;

public class MainActivity extends BaseDrawerActivity {

    TabbarItemProvider tabbarItemProvider;
    TabbarNavigationListener tabbarNavigationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Config config = GlobalConfig.getInstance(this);
        config.setFirstRunDone();

        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        tabbarItemProvider = new TabbarItemProviderImpl(); // this
        tabbarNavigationListener = new TabbarNavigationListenerImpl();
        setupBottomNavigation();

        Fragment initalFragment = tabbarItemProvider.getInitialFragment();
        if(initalFragment != null) {
            Navigation.showFragmentFromActivityInView(initalFragment, this, R.id.activity_main);
        }
        else {
            TabbarButton first = tabbarItemProvider.getTabbarItems().get(0);
            getBottomNavigationView().setSelectedItemId(first.getId());
            tabbarNavigationListener.onTabbarButtonClick(first, this);
        }

        //hidePoweredByWaterLogo();

        EventBus.getDefault().register(this);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if(hasLocationPermission()) {
//            if(!isLocationServiceRunning(LocationService.class))
//            startLocationService();
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // stopLocationService();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNavigationShowFragmentEvent(ShowFragmentNavigationEvent event) {
        Navigation.showFragmentFromActivityInView(event.getFragment(), this, R.id.activity_main);
    }

    // --------------------------------------------------------------------------------------------
    //  Bottom Navigation View
    // --------------------------------------------------------------------------------------------

    void setupBottomNavigation() {
        List<TabbarButton> tabbarButtons = tabbarItemProvider.getTabbarItems();
        setTabbarButtons(tabbarButtons);
        setBigTabbarButton(tabbarItemProvider.getBigButton());
        setTabbarNavigationListener(tabbarNavigationListener);
        prepareTabbarNavigation();

        // By default the first element of the navigation is selected.
        // We dont want this.
        // Select the element under the big button. Its hiden.
        getBottomNavigationView().getMenu().getItem(tabbarButtons.size() / 2).setChecked(true);
    }


    // EVENTS

//    // LOCATION
//
//    @Override
//    public void onPositionChanged(Location location) {
//        de.i42.location.model.Location dbLocation = new de.i42.location.model.Location();
//        dbLocation.latitude = location.getLatitude();
//        dbLocation.longitude = location.getLongitude();
//        dbLocation.timestamp = location.getTime();
//        dbLocation.save();
//    }
//
//    // ---------------------------------------------------------------------------------------------
//    // Location
//    // ---------------------------------------------------------------------------------------------
//
//    private void startLocationService() {
//        Intent service = new Intent(this, LocationService.class);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(service);
//        }
//        else {
//            startService(service);
//        }
//    }
//
//    private void stopLocationService() {
//        Intent service = new Intent(this, LocationService.class);
//        stopService(service);
//    }
//
//    private boolean hasLocationPermission() {
//        return checkCallingOrSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
//                checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private boolean isLocationServiceRunning(Class<?> serviceClass) {
//        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }
}
