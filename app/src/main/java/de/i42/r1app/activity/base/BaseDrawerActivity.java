package de.i42.r1app.activity.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.i42.mdm.notfallapp.core.menu.DrawerNavigationItem;
import de.i42.mdm.notfallapp.core.menu.DrawerNavigationItemProvider;
import de.i42.mdm.notfallapp.core.util.CustomTypefaceSpan;
import de.i42.mdm.notfallapp.core.util.GlobalConfig;
import de.i42.mdm.notfallapp.core.util.SoftNavigationHelper;

import de.i42.r1app.R;
import de.i42.r1app.menu.DrawerNavigationItemProviderImpl;
import de.i42.r1app.tabbar.TabbarButton;
import de.i42.r1app.tabbar.TabbarButtonBig;
import de.i42.r1app.tabbar.TabbarNavigationListener;
import de.i42.r1app.util.BottomNavigationViewHelper;


abstract public class BaseDrawerActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener  {

    DrawerLayout drawerLayout;
    NavigationView drawerNavigationView;
    Toolbar toolbar;
    RelativeLayout contentContainer;
    BottomNavigationView bottomNavigationView;
    ImageButton burgerMenuButton;
    List<DrawerNavigationItem> drawerNavigationItems;

    List<TabbarButton> tabbarButtons;
    TabbarButtonBig bigTabbarButton;
    TabbarNavigationListener tabbarNavigationListener;
    ImageButton bigButton;

    ImageView poweredByWaterLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base_drawer);

        drawerLayout = findViewById(R.id.base_drawer_layout);
        drawerNavigationView = findViewById(R.id.base_drawer_navigation_view);
        contentContainer = findViewById(R.id.base_content_container);
        toolbar = findViewById(R.id.base_actionbar_drawer);
        burgerMenuButton = findViewById(R.id.base_actionbar_drawer_burger);
        bottomNavigationView = findViewById(R.id.base_bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setItemIconTintList(null);
        bigButton = findViewById(R.id.base_bottom_navigation_big_button);
        final Context context = this;
        bigButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tabbarNavigationListener != null) {
                    tabbarNavigationListener.onTabbarButtonClick(bigTabbarButton, context);
                }
                setBigButtonActive();
            }
        });

        //RelativeLayout drawerHeaderLayout = (RelativeLayout) drawerNavigationView.inflateHeaderView(R.layout.drawer_header);
        drawerLayout.addDrawerListener(drawerListener);
        drawerNavigationView.setNavigationItemSelectedListener(navigationItemSelectedListener);

        poweredByWaterLogo = findViewById(R.id.drawer_header_water_logo);


        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if(SoftNavigationHelper.isNavigationBarAvailable()) {
            int bottomMargin = (int) getResources().getDimension(R.dimen.drawer_bottom_logo_container_margin_bottom);
            LinearLayout bottomLogoContainer = findViewById(R.id.base_drawer_bottom_water_logo_container);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) bottomLogoContainer.getLayoutParams();
            params.setMargins(0, 0, 0, bottomMargin);
            bottomLogoContainer.setLayoutParams(params);
        }

        // Prebuild drawer menu
        buildDrawerNavigationItems();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Go home
        if(item.getItemId() == android.R.id.home) {
            /*Intent intent = new Intent(this, TPAFragment.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true; */
        };
        return super.onOptionsItemSelected(item);
    }

    // --------------------------------------------------------------------------------------------
    //  Bottom Navigation View
    // --------------------------------------------------------------------------------------------


    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }

    public void setTabbarButtons(List<TabbarButton> tabbarButtons) {
        this.tabbarButtons = tabbarButtons;
        Collections.sort(tabbarButtons, new Comparator<TabbarButton>() {
            @Override
            public int compare(TabbarButton t1, TabbarButton t2) {
                return Integer.compare(t1.getOrder(), t2.getOrder());
            }
        });
    }

    public void setBigTabbarButton(TabbarButtonBig bigTabbarButtonButton) {
        this.bigTabbarButton = bigTabbarButtonButton;
        bigButton.setImageResource(bigTabbarButtonButton.getInactiveResourceId());
    }

    public void setTabbarNavigationListener(TabbarNavigationListener tabbarNavigationListener) {
        this.tabbarNavigationListener = tabbarNavigationListener;
    }

    @SuppressLint("RestrictedApi")
    public void prepareTabbarNavigation() {
        Menu menu = bottomNavigationView.getMenu();
        menu.clear();
        for (TabbarButton tabbarButton : tabbarButtons) {
            menu.add(Menu.NONE, tabbarButton.getId(), tabbarButton.getOrder(), tabbarButton.getTitle())
                    .setIcon(tabbarButton.getDrawableRecourceId());

        }

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        BottomNavigationViewHelper.disableShiftModeAndFixPadding(menuView);

        // Resize navigation items
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.icon);
            TabbarButton tabbarButton = tabbarButtons.get(i);
            int iconWidth = tabbarButton.getIconWidth();
            int iconHeight = tabbarButton.getIconHeight();

            // Set the default bottom navigation view icon of the big button to 0
            if(i == tabbarButtons.size() / 2) {
                iconWidth = 0;
                iconHeight = 0;
            }

            BottomNavigationViewHelper.resizeMenuItem(iconView, iconWidth, iconHeight, this);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setBigButtonInactive();
        item.setChecked(true);
        onSelectFragment(item);
        return false;
    }

    protected void onSelectFragment(MenuItem item) {
        for(TabbarButton tabbarButton : tabbarButtons) {
            if(tabbarButton.getId() == item.getItemId()) {
                if(tabbarNavigationListener != null) {
                    tabbarNavigationListener.onTabbarButtonClick(tabbarButton, this);
                    break;
                }
            }
        }
    }

    void setBigButtonActive() {
        bigTabbarButton.setActive();
        bigButton.setImageResource(bigTabbarButton.getActiveResourceId());
    }

    void setBigButtonInactive() {
        bigTabbarButton.setInactive();
        bigButton.setImageResource(bigTabbarButton.getInactiveResourceId());
    }

    // --------------------------------------------------------------------------------------------
    //  Drawer
    // --------------------------------------------------------------------------------------------

    public void onActionBarClickEvent(View view) {
        if(view.getId() == R.id.base_actionbar_drawer_burger) {
            buildDrawerNavigationItems();
            toggleDrawer();
        }
    }

    private void buildDrawerNavigationItems() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            drawerNavigationView.setBackground(getResources().getDrawable(R.drawable.drawer_background, null));
        }
        else {
            drawerNavigationView.setBackground(getResources().getDrawable(R.drawable.drawer_background));
        }
        drawerNavigationView.getMenu().clear();
        DrawerNavigationItemProvider itemsProvider = new DrawerNavigationItemProviderImpl();
        drawerNavigationItems = itemsProvider.getMenuList(this);
        for (DrawerNavigationItem item : drawerNavigationItems) {
            MenuItem menuItem = drawerNavigationView.getMenu().add(
                    Menu.NONE,
                    item.getTag(),
                    item.getSort(),
                    item.getTitle()).setIcon(R.drawable.ic_obergefreiter_small);
            applyFontToMenuItem(menuItem);
        }
        drawerNavigationView.setItemHorizontalPaddingResource(R.dimen.drawer_item_padding);
        drawerNavigationView.setItemIconPaddingResource(R.dimen.drawer_icon_padding);
        drawerNavigationView.setItemIconTintList(null);
        int iconSize = (int) (getResources().getDimension(R.dimen.drawer_icon_size) /
                getResources().getDisplayMetrics().density);
        drawerNavigationView.setItemIconSize(iconSize);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            drawerNavigationView.setItemBackground(getResources().getDrawable(R.drawable.drawer_item_background, null));
        }
        else {
            drawerNavigationView.setItemBackground(getResources().getDrawable(R.drawable.drawer_item_background));
        }
    }

    private void toggleDrawer() {
        if(drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        }
        else {
            drawerLayout.openDrawer(GravityCompat.END);
        }
    }

    private void applyFontToMenuItem(MenuItem menuItem) {
        Typeface font = Typeface.createFromAsset(getAssets(), GlobalConfig.getInstance(this).getPrimaryFont());
        SpannableString newTitle = new SpannableString(menuItem.getTitle().toString().toUpperCase());
        newTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , newTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        newTitle.setSpan(new RelativeSizeSpan(1.25f), 0, newTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        newTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorMenuItem)), 0, newTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        menuItem.setTitle(newTitle);
    }

    public void hideBurgerMenuButton() {
        burgerMenuButton.setVisibility(View.GONE);
    }

    public void showBurgerMenuButton() {
        burgerMenuButton.setVisibility(View.VISIBLE);
    }

    // ------------------
    //  Drawer Listeners

    private NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            for(DrawerNavigationItem drawerItem : drawerNavigationItems) {
                if(drawerItem.getTag() == item.getItemId()) {
                    if (drawerItem.getIntent() != null) {
                        startActivity(drawerItem.getIntent());
                    }
                    toggleDrawer(); // hide drawer
                }
            }
            return true;
        }
    };

    private DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(@NonNull View drawerView) {
            buildDrawerNavigationItems();
        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };


    // --------------------------------------------------------------------------------------------
    //  Set/Add Content View
    // --------------------------------------------------------------------------------------------

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(layoutResID, null);
        _setContentView(view, getDefaultLayoutParams());
    }

    @Override
    public void setContentView(View view) {
        _setContentView(view, getDefaultLayoutParams());
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        _setContentView(view, params);
    }

    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params) {
        _setContentView(view, params);
    }

    private ViewGroup.LayoutParams getDefaultLayoutParams() {
        return new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void _setContentView(View view, ViewGroup.LayoutParams params) {
        if(contentContainer == null) {
            throw new NullPointerException("BaseDrawerActivity: 'contentContainer' is NULL. You need to call BaseDrawerActivity.onCreate() first, before you can do any layout changes.");
        }
        contentContainer.addView(view, params);
    }


    // App Name
    // TODO: Refactor to Application-Class?
    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    // --------------------------------------------------------------------------------------------
    //  Powered By water logo
    // --------------------------------------------------------------------------------------------

    public void hidePoweredByWaterLogo() {
        poweredByWaterLogo.setVisibility(View.GONE);
    }
}
